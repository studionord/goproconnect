# goproconnect [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> connect gopro from raspberry pi for taking timelapse photos


## Install

```sh
$ npm install --save goproconnect
```


## Usage

```js
var goproconnect = require('goproconnect');

goproconnect('Rainbow');
```

## License

MIT © [Gürkan Tümer](http://www.studionord.org)


[npm-image]: https://badge.fury.io/js/goproconnect.svg
[npm-url]: https://npmjs.org/package/goproconnect
[travis-image]: https://travis-ci.org/gurkantumer/goproconnect.svg?branch=master
[travis-url]: https://travis-ci.org/gurkantumer/goproconnect
[daviddm-image]: https://david-dm.org/gurkantumer/goproconnect.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/gurkantumer/goproconnect
