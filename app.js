var when = require('when');
var async = require('async');
var sleep = require('sleep');
var request = require('request');

// parameters
var goProIP = '10.5.5.9';
var pass = 'goprohero';

var sleepVar = 10;
var timelapseVar = 60;

/*power up */				         var url1 = 'http://' + goProIP + '/bacpac/PW?t=' + pass + '&p=%01';
/*change to picture mode*/	 var url2 = 'http://' + goProIP + '/camera/CM?t=' + pass + '&p=%01';
/* take pic */ 				       var url3 = 'http://' + goProIP + '/bacpac/SH?t=' + pass + '&p=%01';
/*power down*/ 				       var url4 = 'http://' + goProIP + '/bacpac/PW?t=' + pass + '&p=%00';

function powerUpAndTakeShot(callback, errorCallback) {
	async.waterfall([
      function(cb){
      	console.log("power on :" + url1);
      	request({
    		method: 'GET',
    		uri: url1
  		}, function (error, res, body) {
   			console.log(error);
   			if (!error) {
          console.log("ok");
   				sleep.sleep(sleepVar);
   				cb(null);
   			}else{
          console.log("error 1");
   				errorCallback();
   			};
  		});	        
      },
      function(cb){
      	console.log("change mode :" + url2);
      	request({
    		method: 'GET',
    		uri: url2
  		}, function (error, res, body) {
  			console.log(error);
   			if (!error) {
          console.log("ok");
   				sleep.sleep(sleepVar);
   				cb(null);
   			}else{
          console.log("error 2");
   				errorCallback();
   			};
   			
  		});
      },
      function(cb){
      	console.log("take picture :" + url3);
      	request({
    		method: 'GET',
    		uri: url3
  		}, function (error, res, body) {
   			console.log(error);
   			if (!error) {
          console.log("ok");
   				sleep.sleep(sleepVar);
   				cb(null);
   			}else{
          console.log("error 3");
   				errorCallback();
   			};
  		});
      },
      function(cb) {
      	console.log("power off :" + url4);
      	request({
    		method: 'GET',
    		uri: url4
  		}, function (error, res, body) {
   			console.log(error);
   			if (!error) {
          console.log("ok");
   				sleep.sleep(sleepVar);
   				callback();
   			}else{
          console.log("error 4");
   				errorCallback();
   			};
  		});
        
      }
    ]);
	/*return snap()
	.then(function() {
	})
	.then(function() {
	})*/
}

function loop() {
	powerUpAndTakeShot(function(data){
      sleep.sleep(timelapseVar);
      console.log("sleep + loop one more time");
      loop();

    },function(){
      console.log("error occured during picture taking");
    });
}

function configure(){
        var args = process.argv.slice(2);

        //sleepVar = args[0];
       // timelapseVar = args[1];
        console.log("sleepVar:"+sleepVar);
        console.log("timelapseVar:"+timelapseVar);

        loop();
};

configure();
